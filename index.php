<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name : {$sheep->name}<br>"; // "shaun"
echo "Legs : {$sheep->legs}<br>"; // 4
echo "Cold_blooded: {$sheep->cold_blooded}<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : {$sungokong->name}<br>"; // "shaun"
echo "Legs : {$sungokong->legs}<br>"; // 4
echo "Cold_blooded: {$sungokong->cold_blooded}<br>";
echo " {$sungokong->yell()} <br><br>"; // "Auooo"

$kodok = new Frog("buduk");  

echo "Name : {$kodok->name}<br>"; // "shaun"
echo "Legs : {$kodok->legs}<br>"; // 4
echo "Cold_blooded: {$kodok->cold_blooded}<br>";
echo "{$kodok->jump()} <br>" ; // "hop hop"